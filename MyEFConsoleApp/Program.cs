﻿using System;

namespace MyEFConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            MyContext context = new MyContext();
            Employee emp = new Employee() {  EmployeeName = "Hari", Address = "Hyderabad" };
            context.employees.Add(emp);
            context.SaveChanges();

            Console.WriteLine("Hello World!");
        }
    }
}
